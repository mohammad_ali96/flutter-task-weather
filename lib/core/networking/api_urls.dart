import 'package:weather_task/core/utils/constant_utils.dart';


class ApiUrls {
  static const _baseUri = "api.openweathermap.org";
  static const _apiVersion = "2.5";
  static const apiKey = "cb31958e8627146e6a560af562bd9482";
  static const baseUrl = 'https://$_baseUri/data/$_apiVersion/';

  static Uri login({Map<String, dynamic>? params}) {
    String query = buildQuery(params);
    String url = '$baseUrl/login$query';
    return Uri.parse(url);
  }

  static Uri weatherDetails({Map<String, dynamic>? params}) {

    String query = buildQuery(params);
    String url = '$baseUrl/forecast$query';
    return Uri.parse(url);
  }
}
