import 'dart:io';
import 'package:weather_task/core/utils/shared_preferences.dart';

abstract class ApiSettings {
  static final Map<String, String> _headers = {};

  static void initHeaders() {
    _headers.putIfAbsent(
        HttpHeaders.contentTypeHeader, () => 'application/json');
    _headers.putIfAbsent(HttpHeaders.acceptHeader, () => 'application/json');
  }

  static void clearHeaders() {
    _headers.clear();
  }

  static Map<String, String> authorizationHeader() {
    return {
      HttpHeaders.authorizationHeader: "Bearer ${SharedPref().getApiToken()}"
    };
  }

  static Map<String, String> getCustomHeaders() {
    return _headers;
  }

  static Map<String, String> getAuthorizedHeaders() {
    Map<String, String> headers = {};
    headers.addAll(getCustomHeaders());
    headers.addAll(authorizationHeader());
    return headers;
  }
}
