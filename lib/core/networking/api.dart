import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:weather_task/core/networking/response.dart';
import 'package:weather_task/core/utils/handlers/connection_status_handler.dart';
import 'package:weather_task/core/utils/my_navigator.dart';
import 'package:http/http.dart' as http;

import 'api_constants.dart';
import 'api_settings.dart';

class Api {
  static Future<Response> get(Uri uri, BuildContext context) async {
    try {
//      if (!ConnectionStatusHandler().hasConnection) {
//        return Response(ApiCode.kTimeOutDuration, {
//          "message": ApiMessages.kErrorNoInternetMessage,
//        });
//      }

      http.Response response = await http
          .get(uri, headers: ApiSettings.getCustomHeaders())
          .timeout(const Duration(seconds: ApiCode.kTimeOutDuration));

      debugPrint('status code get Model ${response.statusCode}');
      debugPrint('body of get Model ${response.body}');
      var data = json.decode(response.body);
      if (response.statusCode == ApiCode.kSuccessCode) {
        return Response(ApiCode.kSuccessCode, data);
      } else if (response.statusCode == ApiCode.kUnAuthorizedRequestCode ||
          response.statusCode == ApiCode.kForbiddenRequestCode) {

        MyNavigator(context).logOut();
        return Response(response.statusCode, null);
      } else {
        return Response(response.statusCode, data);
      }
    } on TimeoutException catch (_) {
      return _getTimeOutExceptionResponse();
    } catch (exception) {
      debugPrint("exception $exception");
      return _getExceptionResponse();
    }
  }

  static Response _getExceptionResponse() {
    return Response(ApiCode.kErrorExceptionCode, {
      "message": ApiMessages.kErrorExceptionMessage,
    });
  }

  static Response _getTimeOutExceptionResponse() {
    return Response(ApiCode.kErrorTimeOutCode,
        {"message": ApiMessages.kErrorNoInternetMessage});
  }
}
