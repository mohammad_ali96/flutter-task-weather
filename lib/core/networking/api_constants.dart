abstract class ApiMessages {
  static const kErrorNoInternetMessage = 'Please check your connection.';
  static const String kErrorExceptionMessage =
      "Something error, please try again.";
  static const String kNoDataMessage = "No data found.";
}

abstract class ApiCode {
  static const int kTimeOutDuration = 30;
  static const int kSuccessCode = 200;
  static const int kNoDataCode = 204;
  static const int kErrorServerCode = 500;
  static const int kErrorBadRequestCode = 400;
  static const int kUnProcessableRequestCode = 422;
  static const int kUnAuthorizedRequestCode = 401;
  static const int kForbiddenRequestCode = 403;
  static const int kErrorExceptionCode = -1;
  static const int kErrorTimeOutCode = -3;
}
