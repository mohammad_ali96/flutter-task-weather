class CustomApiResult {
  bool success;
  dynamic data;

  Map<String, dynamic>? errors;

  String? message;

  int? code;


  CustomApiResult(
      {required this.success, required this.data, required this.message});

  CustomApiResult.fromJson(Map<String, dynamic> json)
      : success = json.containsKey('success') ? json['success'] : false,
        data = json.containsKey('data') ? json['data'] : null,
        code =
            json.containsKey('cod') ? int.parse(json['cod'].toString()) : null,
        errors = json.containsKey('errors') ? json['errors'] : null,
        message = json.containsKey('message') ? json['message'].toString() : null;
}
