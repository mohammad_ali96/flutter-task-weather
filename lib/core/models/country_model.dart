import 'coordinate_model.dart';

class CountryModel {
  int id;
  String name;
  String country;
  int population;
  int timezone;

  CoordinateModel? coordinate;

  CountryModel.fromJson(Map<String, dynamic> json)
      : id = json.containsKey("id") ? json['id'] : 0,
        name = json.containsKey("name") ? json['name'] : '',
        country = json.containsKey("country") ? json['country'] : '',
        population = json.containsKey("population") ? json['population'] : 0,
        timezone = json.containsKey("timezone") ? json['timezone'] : 0,
        coordinate = json.containsKey("coord") && json["coord"] != null
            ? CoordinateModel.fromJson(json["coord"])
            : null;
}
