class CoordinateModel {
  double lat;
  double lon;

  CoordinateModel.fromJson(Map<String, dynamic> json)
      : lat = json.containsKey("lat") ? json['lat'] : 0.0,
        lon = json.containsKey("lon") ? json['lon'] : 0.0;
}
