import 'package:intl/intl.dart';

String buildQuery(Map<String, dynamic>? param) {
  if (param == null) return "";
  String query = "?";
  for (int i = 0; i < param.length; i++) {
    String key = param.keys.elementAt(i);
    dynamic value = param.values.elementAt(i);
    query += "$key=$value&";
  }
// delete &
  query = query.substring(0, query.length - 1);
  return query;
}

String getNameOfDayFromDateAsString(String dateAsString) {
  try {
    DateTime date = DateTime.parse(dateAsString);
    String day = DateFormat('EE').format(date);
    return day;
  } catch (e) {
    return "";
  }
}

String getDateFromDateAsString(String dateAsString) {
  try {
    DateTime date = DateTime.parse(dateAsString);
    String day = DateFormat('dd/MM/yyyy').format(date);
    return day;
  } catch (e) {
    return "";
  }
}

String getTimeFromDateAsString(String dateAsString) {
  try {
    DateTime date = DateTime.parse(dateAsString);
    String day = DateFormat('hh:mm').format(date);
    return day;
  } catch (e) {
    return "";
  }
}
String getDateFromDayAndMonthAsString(String dateAsString) {
  try {
    DateTime date = DateTime.parse(dateAsString);
    String day = DateFormat('dd/MM').format(date);
    return day;
  } catch (e) {
    return "";
  }
}


String getCelsiusSign() => "  \u2103";
