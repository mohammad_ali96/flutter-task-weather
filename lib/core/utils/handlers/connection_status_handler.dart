import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';
import 'package:weather_task/core/dialogs/dialogs_manager.dart';
import 'package:weather_task/core/dialogs/no_internet_dialog.dart';
import 'package:weather_task/core/utils/constants/public_constants.dart';

class ConnectionStatusHandler {
  static final ConnectionStatusHandler _singleton =
      ConnectionStatusHandler._internal();

  factory ConnectionStatusHandler() {
    return _singleton;
  }

  ConnectionStatusHandler._internal();

  static const googleUrl = 'google.com';

  static ConnectivityResult? connectivityResult;

  StreamController? connectionChangeController;

  Connectivity? _connectivity;

  BuildContext? mainContext;

  bool hasConnection = false;
  bool isDialogOpen = false;

  void initialize() {
    connectionChangeController = StreamController.broadcast();
    _connectivity = Connectivity();
    _connectivity!.onConnectivityChanged.listen(_connectionChange);
    checkConnection();
  }

  void setDialogContext(context) {
    mainContext = context;
  }

  static bool get hasInternetConnection =>
      connectivityResult != null &&
      connectivityResult != ConnectivityResult.none;

  void _connectionChange(ConnectivityResult result) {
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
        checkConnection();
        break;
      case ConnectivityResult.none:
        hasConnection = false;
        showNoConnectionDialog();
        break;
    }
    connectivityResult = result;
  }

  Future<bool> checkConnection() async {
    bool previousConnection = hasConnection;
    try {
      final result = await InternetAddress.lookup(googleUrl);
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        hasConnection = true;
      } else {
        hasConnection = false;

        showNoConnectionDialog();
      }
    } on SocketException catch (e) {
      hasConnection = false;
      showNoConnectionDialog();
      debugPrint(e.toString());
    }
    if (previousConnection != hasConnection) {
      connectionChangeController!.add(hasConnection);
    }

    return hasConnection;
  }

  void dispose() {
    connectionChangeController!.close();
  }

  void showNoConnectionDialog() async {
    if (isDialogOpen || mainContext == null) return;
    isDialogOpen = true;
    await showCustomDialog(mainContext,
        title: kNoConnectionTXT, page: const NoInternetDialog());
    isDialogOpen = false;
  }
}
