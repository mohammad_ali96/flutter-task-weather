import 'package:shared_preferences/shared_preferences.dart';

class SharedPref {
  static SharedPref? _instance;

  SharedPreferences? _prefs;

  SharedPref._createInstance();

  /// Checks if shared preference is initialized
  static Future<SharedPref?> init() async {
    _instance ??= SharedPref._createInstance();
    _instance!._prefs ??= await SharedPreferences.getInstance();
    return _instance;
  }

  factory SharedPref() {
    return _instance!;
  }

  void resetSharedPreference() {
    removeApiToken();
  }

  void logOut() async {
    resetSharedPreference();
  }

  String? getApiToken() {
    return _instance!._prefs!.getString("apiToken") ?? "";
  }

  void saveApiToken(String apiToken) {
    _instance!._prefs!.setString("apiToken", apiToken);
  }

  void removeApiToken() {
    _instance!._prefs!.remove("apiToken");
  }
}
