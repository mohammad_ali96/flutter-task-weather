import 'package:flutter/material.dart';
import 'package:weather_task/core/screens/screen_utils/page_navigator_transition/slide_right_route.dart';
import 'package:weather_task/core/utils/shared_preferences.dart';
import 'package:weather_task/features/weather_forecast/data/models/weather_forecast_model.dart';
import 'package:weather_task/features/weather_forecast/presentation/screens/weather_day_details_sreen.dart';

class MyNavigator {
  final BuildContext? context;

  MyNavigator(this.context);

  openPage(page) {
    Navigator.push(context!, SlideRightRoute(page: page));
  }

  Future<void> openPageWithAsync(page) async {
    await Navigator.push(context!, SlideRightRoute(page: page));
    return;
  }

  openPageAndRemoveUntil(page) {
    Navigator.of(context!).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => page),
        (Route<dynamic> route) => false);
  }

  closePage() {
    Navigator.of(context!).pop();
  }

  logOut() {
    SharedPref().logOut();
//    popAllPagesAndGoToSiginPage();
  }

  goToWeatherDetailsPage({required WeatherForecastModel weather}) {
    openPage(WeatherDayDetails(weather: weather));
  }
}
