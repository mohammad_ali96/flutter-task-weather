class ImagesRes {
  static const String _rootIcons = 'assets/icons/public/';

  static const String arrowBack = _rootIcons + 'arrow_back.png';
}

class FontsRes {
  static const String segeoFont = 'Segeo';
}
