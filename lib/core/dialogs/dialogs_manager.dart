import 'package:flutter/material.dart';
import 'package:weather_task/core/utils/thems/colors_repository.dart';

Future<dynamic> showCustomDialog(
  mainContext, {
  bool barrierDismissible = false,
  isBuilderChild = true,
  String? title,
  required Widget page,
}) async {
  return isBuilderChild == true
      ? showDialog<dynamic>(
          context: mainContext,
          barrierDismissible: barrierDismissible,
          builder: (BuildContext context) {
            return CustomDialog(
              title: title,
              page: page,
            );
          },
        )
      : showDialog<dynamic>(
          context: mainContext,
          barrierDismissible: barrierDismissible,
          builder: (BuildContext context) {
            return page;
          });
}

class CustomDialog extends StatelessWidget {
  final String? title;

  final Widget page;

  const CustomDialog({Key? key, required this.title, required this.page})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return AlertDialog(
      backgroundColor: Colors.white,
      insetPadding: const EdgeInsets.only(left: 18, right: 18),
      shape: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(16.0)),
          borderSide: BorderSide.none),
      titlePadding: const EdgeInsets.all(0),
      title: Container(
          width: width,
          height: 46,
          decoration: const BoxDecoration(
              gradient:
                  LinearGradient(colors: [ColorsRes.kBlue, ColorsRes.kBlue]),
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(16), topLeft: Radius.circular(16))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 16.0, right: 16),
                  child: Text(
                    title ?? "",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: const TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w500),
                  ),
                ),
              ),
              IconButton(
                  icon: const Icon(
                    Icons.close,
                    color: Colors.white,
                    size: 22,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  })
            ],
          )),
      content: page,
      contentPadding: const EdgeInsets.only(bottom: 16.0, left: 16, right: 16),
    );
  }
}
