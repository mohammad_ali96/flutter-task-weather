import 'package:weather_task/core/models/country_model.dart';
import 'package:weather_task/features/weather_forecast/data/models/weather_forecast_model.dart';

class WeatherCity {
  CountryModel? city;

  List<WeatherForecastModel>? list;

  WeatherCity.fromJson(Map<String, dynamic> json)
      : city = json.containsKey("city") && json["city"] != null
            ? CountryModel.fromJson(json["city"])
            : null,
        list = json.containsKey("list") && json["list"] != null
            ? (json["list"] as List)
                .map((e) => WeatherForecastModel.fromJson(e))
                .toList()
            : null;
}
