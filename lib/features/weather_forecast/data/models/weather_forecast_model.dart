import 'package:weather_task/core/utils/constant_utils.dart';
import 'package:weather_task/features/weather_forecast/data/models/weather_model.dart';

class WeatherForecastModel {
  String? time;
  String? day;
  String? date;
  String? dayAndMonth;
  String? dateAsString;

  WeatherModel? weather;

  WeatherMainInfo? weatherMainInfo;

  List<WeatherForecastModel> weatherForeastSameDayList = [];


  WeatherForecastModel();

  WeatherForecastModel.fromJson(Map<String, dynamic> json)
      : dateAsString = json.containsKey("dt_txt") ? json['dt_txt'] : '',
        time = json.containsKey("dt_txt")
            ? getTimeFromDateAsString(json['dt_txt'])
            : '',
        day = json.containsKey("dt_txt")
            ? getNameOfDayFromDateAsString(json['dt_txt'])
            : '',
        date = json.containsKey("dt_txt")
            ? getDateFromDateAsString(json['dt_txt'])
            : '',
        dayAndMonth = json.containsKey("dt_txt")
            ? getDateFromDayAndMonthAsString(json['dt_txt'])
            : '',
        weather = json.containsKey("weather") &&
                json["weather"] != null &&
                (json["weather"] as List).isNotEmpty
            ? WeatherModel.fromJson(json["weather"][0])
            : null,
        weatherMainInfo = json.containsKey("main") && json["main"] != null
            ? WeatherMainInfo.fromJson(json["main"])
            : null;
}

class WeatherMainInfo {
  int temp;
  int tempMin;
  int tempMax;

  WeatherMainInfo.fromJson(Map<String, dynamic> json)
      : temp = json.containsKey("temp") ? json['temp'].ceil() : 0,
        tempMin = json.containsKey("temp_min")
            ? json['temp_min'].ceil()
            : 0,
        tempMax =
            json.containsKey("temp_max") ? json['temp_max'].ceil()  : 0;
}
