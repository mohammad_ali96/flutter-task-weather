class WeatherModel {
  int id;
  String main;
  String description;
  String icon;

  WeatherModel.fromJson(Map<String, dynamic> json)
      : id = json.containsKey("id") ? json['id'] : 0,
        main = json.containsKey("main") ? json['main'] : '',
        description =
            json.containsKey("description") ? json['description'] : '',
        icon = json.containsKey("icon") ? getIconUrl(json['icon']) : '';
}

String getIconUrl(String? iconCode) {
  if (iconCode != null) {
    return "http://openweathermap.org/img/w/$iconCode.png";
  } else {
    return "";
  }
}
