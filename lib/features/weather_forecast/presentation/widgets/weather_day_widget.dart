import 'package:flutter/material.dart';
import 'package:weather_task/core/utils/constant_utils.dart';
import 'package:weather_task/core/utils/my_navigator.dart';
import 'package:weather_task/features/weather_forecast/data/models/weather_forecast_model.dart';

class WeatherDayWidget extends StatelessWidget {
  final WeatherForecastModel weather;

  const WeatherDayWidget({Key? key, required this.weather}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        MyNavigator(context).goToWeatherDetailsPage(weather: weather);
      },
      child: Container(
        width: 90,
        height: 184,
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
            color: Colors.grey.shade100,
            borderRadius: BorderRadius.all(Radius.circular(38))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              weather.day ?? "",
              style: const TextStyle(
                fontSize: 14,
                color: Colors.black54,
              ),
            ),
            const SizedBox(
              height: 4,
            ),
            Text(
              weather.dayAndMonth ?? "",
              style: const TextStyle(
                fontSize: 14,
                color: Colors.black54,
              ),
            ),
            const SizedBox(
              height: 6,
            ),
            Image.network(
              weather.weather!.icon,
              height: 38,
            ),
            const SizedBox(
              height: 4,
            ),
            Text(
              weather.weather!.main,
              style: const TextStyle(
                fontSize: 14,
                color: Colors.black54,
              ),
            ),
            const SizedBox(
              height: 4,
            ),
            Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    weather.weatherMainInfo!.tempMin.toString() +
                        "/" +
                        weather.weatherMainInfo!.tempMax.toString(),
                    style: const TextStyle(
                        fontSize: 14,
                        color: Colors.black54,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    getCelsiusSign(),
                    style: const TextStyle(
                      fontSize: 9,
                      color: Colors.black54,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
