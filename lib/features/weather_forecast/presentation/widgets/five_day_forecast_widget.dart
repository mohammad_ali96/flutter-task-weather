import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_task/core/utils/constants/weather_constant.dart';
import 'package:weather_task/features/weather_forecast/presentation/bloc/weather.dart';
import 'package:weather_task/features/weather_forecast/presentation/widgets/weather_day_widget.dart';

class FiveDaysForecastWidget extends StatelessWidget {
  const FiveDaysForecastWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: const [
            Padding(
              padding: EdgeInsets.only(left: 32.0, right: 32),
              child: Text(
                kFiveDaysForeastTXT,
                textAlign: TextAlign.start,
                style: TextStyle(
                    fontSize: 24,
                    color: Colors.black54,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 22,
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(16),
          child: Row(
            children: BlocProvider.of<WeatherListBloc>(context)
                .state
                .weatherForecastList
                .map((weather) => Padding(
                  padding: const EdgeInsets.only(left: 6.0, right: 6),
                  child: WeatherDayWidget(
                        weather: weather,
                      ),
                ))
                .toList(),
          ),
        )
      ],
    );
  }
}
