import 'package:flutter/material.dart';
import 'package:weather_task/core/utils/constant_utils.dart';
import 'package:weather_task/features/weather_forecast/data/models/weather_forecast_model.dart';

class TimeWeatherWidget extends StatelessWidget {
  final WeatherForecastModel weather;

  const TimeWeatherWidget({Key? key, required this.weather}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.network(
              weather.weather!.icon,
              height: 36,
            ),
            const SizedBox(
              width: 16,
            ),
            Text(
              weather.time!,
              style: const TextStyle(fontSize: 16, color: Colors.black54),
            ),
            const SizedBox(
              width: 16,
            ),
            Text(
              weather.weather!.main,
              style: const TextStyle(fontSize: 16, color: Colors.black54),
            ),
          ],
        ),
        Row(
          children: [
            Text(
              weather.weatherMainInfo!.temp.toString(),
              style: const TextStyle(
                  fontSize: 20,
                  color: Colors.black54,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              getCelsiusSign(),
              style: const TextStyle(
                fontSize: 14,
                color: Colors.black54,
              ),
            ),
          ],
        )
      ],
    );
  }
}
