import 'package:flutter/material.dart';
import 'package:weather_task/core/utils/constant_utils.dart';
import 'package:weather_task/core/utils/my_navigator.dart';
import 'package:weather_task/core/utils/thems/colors_repository.dart';
import 'package:weather_task/features/weather_forecast/data/models/weather_forecast_model.dart';

class WeatherDetailWidget extends StatelessWidget {
  final WeatherForecastModel weather;

  const WeatherDetailWidget({Key? key, required this.weather})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        MyNavigator(context).goToWeatherDetailsPage(weather: weather);
      },
      child: Column(
        children: [
          _buildCityNameWithDate("Dubia", weather.day, weather.date),
          const SizedBox(
            height: 16,
          ),
          _buildTempWidget(
              weather.weather!.main,
              weather.weatherMainInfo!.tempMax,
              weather.weatherMainInfo!.tempMin),
          const SizedBox(
            height: 62,
          ),
          _buildIconWithTempWidget(
              context, weather.weather!.icon, weather.weatherMainInfo!.temp),
        ],
      ),
    );
  }

  Widget _buildCityNameWithDate(String cityName, String? day, String? date) {
    return Padding(
      padding: const EdgeInsets.only(left: 32.0, right: 32),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            cityName,
            style: const TextStyle(fontSize: 20, color: Colors.black54),
          ),
          Text(
            day! + " " + date!,
            style: const TextStyle(fontSize: 20, color: Colors.black54),
          )
        ],
      ),
    );
  }

  Widget _buildTempWidget(String? main, int? tempMax, int? tempMin) {
    return Padding(
      padding: const EdgeInsets.only(left: 32.0, right: 32),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            main ?? "",
            style: const TextStyle(
                fontSize: 24,
                color: Colors.black54,
                fontWeight: FontWeight.bold),
          ),
          Row(
            children: [
              Text(
                tempMax.toString() + " / " + tempMin.toString(),
                style: const TextStyle(
                    fontSize: 24,
                    color: Colors.black54,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                getCelsiusSign(),
                style: const TextStyle(
                  fontSize: 14,
                  color: Colors.black54,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildIconWithTempWidget(context, String icon, int temp) {
    double width = MediaQuery.of(context).size.width;
    return Padding(
      padding: const EdgeInsets.only(left: 32.0, right: 32),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
              child: Material(
            elevation: 16,
            type: MaterialType.circle,
            color: ColorsRes.kOrange,
            shadowColor: ColorsRes.kOrange,
            child: CircleAvatar(
              backgroundColor: ColorsRes.kOrange,
              radius: width / 8,
            ),
          )),
          const SizedBox(
            width: 32,
          ),
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                  child: Text(
                    temp.toString(),
                    style: const TextStyle(
                        fontSize: 85,
                        color: Colors.black54,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 12.0),
                  child: Text(
                    getCelsiusSign(),
                    style: const TextStyle(
                      fontSize: 25,
                      color: Colors.black54,
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
