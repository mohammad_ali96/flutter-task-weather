import 'package:equatable/equatable.dart';
import 'package:weather_task/features/weather_forecast/data/models/weather_forecast_model.dart';

class WeatherState extends Equatable {
  final List<WeatherForecastModel> weatherForecastList;

  const WeatherState({required this.weatherForecastList});

  factory WeatherState.initial() => const WeatherState(weatherForecastList: []);

  @override
  List<Object> get props => [weatherForecastList];
}

class WeatherInitialized extends WeatherState {
  WeatherInitialized() : super(weatherForecastList: []);
}

class WeatherIsLoading extends WeatherState {
  const WeatherIsLoading(
      {required List<WeatherForecastModel> weatherForecastList})
      : super(weatherForecastList: weatherForecastList);
}

class WeatherIsLoaded extends WeatherState {
  const WeatherIsLoaded(
      {required List<WeatherForecastModel> weatherForecastList})
      : super(weatherForecastList: weatherForecastList);
}

class WeatherTimeOut extends WeatherState {
  const WeatherTimeOut(
      {required List<WeatherForecastModel> weatherForecastList})
      : super(weatherForecastList: weatherForecastList);
}

class WeatherNoInternet extends WeatherState {
  const WeatherNoInternet(
      {required List<WeatherForecastModel> weatherForecastList})
      : super(weatherForecastList: weatherForecastList);
}

class WeatherErrorFromApp extends WeatherState {
  const WeatherErrorFromApp(
      {required List<WeatherForecastModel> weatherForecastList})
      : super(weatherForecastList: weatherForecastList);
}

class WeatherErrorFromServer extends WeatherState {
  const WeatherErrorFromServer(
      {required List<WeatherForecastModel> weatherForecastList})
      : super(weatherForecastList: weatherForecastList);
}

class WeatherNotSpecifiedError extends WeatherState {
  const WeatherNotSpecifiedError(
      {required List<WeatherForecastModel> weatherForecastList})
      : super(weatherForecastList: weatherForecastList);
}

class WeatherStoresTempState extends WeatherState {
  const WeatherStoresTempState(
      {required List<WeatherForecastModel> weatherForecastList})
      : super(weatherForecastList: weatherForecastList);
}
