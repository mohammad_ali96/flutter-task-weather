import 'package:flutter/material.dart';
import 'package:weather_task/core/networking/api.dart';
import 'package:weather_task/core/networking/api_constants.dart';
import 'package:weather_task/core/networking/api_urls.dart';
import 'package:weather_task/core/networking/response.dart';
import 'package:weather_task/features/weather_forecast/data/models/weather_forecast_model.dart';

class WeatherRepo {
  Future<Response> getWeatherDetails(BuildContext context,
      {Map<String, dynamic>? params}) async {
    Response response =
        await Api.get(ApiUrls.weatherDetails(params: params), context);

    return _parsedArticlesFromJson(response, context);
  }

  Future<Response> _parsedArticlesFromJson(
      Response response, BuildContext context) async {
    try {
      var result = response.object;

      if (response.statusCode == ApiCode.kSuccessCode) {
        return Response(response.statusCode, foldListResponse(result['list']));
      } else {
        return Response(response.statusCode, []);
      }
    } catch (e) {
      print(e.toString());
      return Response(ApiCode.kErrorExceptionCode,
          {"message": ApiMessages.kErrorExceptionMessage});
    }
  }

  List<WeatherForecastModel> foldListResponse(List list) {
    List<WeatherForecastModel> allWeatherForecastList =
        list.map((tmp) => WeatherForecastModel.fromJson(tmp)).toList();
    Set<String> dates = {};

    // find dates
    for (int i = 0; i < allWeatherForecastList.length; i++) {
      dates.add(allWeatherForecastList.elementAt(i).date!);
    }

    // initialize weatherForecastList
    List<WeatherForecastModel> weatherForecastList =
        List.generate(dates.length, (index) => WeatherForecastModel());

    var weatherMap = allWeatherForecastList
        .fold<Map<String, List<WeatherForecastModel>>>({},
            (weatherMap, currentWeather) {
      if (weatherMap[currentWeather.date] == null) {
        weatherMap[currentWeather.date!] = [];
      }
      weatherMap[currentWeather.date]?.add(currentWeather);
      return weatherMap;
    });
    int idx = 0;
    weatherMap.forEach((key, value) {
      weatherForecastList[idx] = weatherMap[key]!.elementAt(0);
      weatherForecastList[idx]
          .weatherForeastSameDayList
          .addAll(weatherMap[key]!);
      weatherForecastList[idx].weatherForeastSameDayList.removeAt(0);
      idx++;
    });
    return weatherForecastList;
  }
}
