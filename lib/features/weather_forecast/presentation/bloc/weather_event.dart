import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class WeatherEvent extends Equatable {
  final BuildContext context;

  const WeatherEvent({
    required this.context,
  });

  @override
  List<Object> get props => [];
}

class FetchWeatherList extends WeatherEvent {
  const FetchWeatherList({required BuildContext context})
      : super(context: context);
}

class FetchWeatherListFromQuery extends WeatherEvent {
  final Map<String, dynamic> params;

  const FetchWeatherListFromQuery(
      {required this.params, required BuildContext context})
      : super(context: context);

  @override
  List<Object> get props => [params];
}

class ResetWeatherList extends WeatherEvent {
  const ResetWeatherList({required BuildContext context}) : super(context: context);
}
