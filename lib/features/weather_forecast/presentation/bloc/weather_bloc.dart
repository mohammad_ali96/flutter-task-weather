import 'package:bloc/bloc.dart';
import 'package:weather_task/core/networking/api_constants.dart';
import 'package:weather_task/core/networking/response.dart';
import 'package:weather_task/features/weather_forecast/data/models/weather_forecast_model.dart';
import 'package:weather_task/features/weather_forecast/presentation/bloc/weather.dart';

class WeatherListBloc extends Bloc<WeatherEvent, WeatherState> {
  WeatherListBloc() : super(WeatherState.initial());

  @override
  Stream<WeatherState> mapEventToState(WeatherEvent event) async* {
    try {
      if (event is FetchWeatherListFromQuery) {
        yield WeatherIsLoading(weatherForecastList: state.weatherForecastList);
        Response response = await WeatherRepo()
            .getWeatherDetails(event.context, params: event.params);

        if (response.statusCode == ApiCode.kSuccessCode) {
          List<WeatherForecastModel> weatherForecastList = response.object;
          yield WeatherIsLoaded(weatherForecastList: weatherForecastList);
        } else if (response.statusCode == ApiCode.kNoDataCode) {
          yield WeatherIsLoaded(weatherForecastList: state.weatherForecastList);
        } else if (response.statusCode == ApiCode.kErrorServerCode) {
          yield WeatherErrorFromServer(
              weatherForecastList: state.weatherForecastList);
        } else if (response.statusCode == ApiCode.kErrorExceptionCode) {
          yield WeatherErrorFromApp(
              weatherForecastList: state.weatherForecastList);
        } else if (response.statusCode == ApiCode.kErrorTimeOutCode) {
          yield WeatherTimeOut(weatherForecastList: state.weatherForecastList);
        } else {
          yield WeatherNotSpecifiedError(
              weatherForecastList: state.weatherForecastList);
        }
      } else if (event is ResetWeatherList) {
        yield WeatherInitialized();
      }
    } catch (e) {
      yield WeatherErrorFromApp(weatherForecastList: state.weatherForecastList);
    }
  }
}
