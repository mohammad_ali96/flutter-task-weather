import 'package:animation_wrappers/animation_wrappers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_task/core/networking/api_urls.dart';
import 'package:weather_task/features/weather_forecast/data/models/weather_forecast_model.dart';
import 'package:weather_task/features/weather_forecast/presentation/bloc/weather.dart';
import 'package:weather_task/features/weather_forecast/presentation/widgets/five_day_forecast_widget.dart';
import 'package:weather_task/features/weather_forecast/presentation/widgets/weather_detail_widgte.dart';

class WeatherDayScreen extends StatefulWidget {
  const WeatherDayScreen({Key? key}) : super(key: key);

  @override
  _WeatherDayScreenState createState() => _WeatherDayScreenState();
}

class _WeatherDayScreenState extends State<WeatherDayScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocProvider(
        create: (context) => WeatherListBloc(),
        child: _BodyWeatherDayScreen(),
      ),
    );
  }
}

class _BodyWeatherDayScreen extends StatefulWidget {
  @override
  _BodyWeatherDayScreenState createState() => _BodyWeatherDayScreenState();
}

class _BodyWeatherDayScreenState extends State<_BodyWeatherDayScreen> {
  final GlobalKey<RefreshIndicatorState> _refreshItem =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies()async {
    super.didChangeDependencies();
    debugPrint("Get Data");
    await getData();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      key: _refreshItem,
      onRefresh: () async {
        await getData();
        return;
      },
      child: FadedSlideAnimation(
        child: ListView(
          padding: const EdgeInsets.only(
            top: 52,
            bottom: 52,
          ),
          children: <Widget>[
            _buildWeatherScreen(),
          ],
        ),
        beginOffset: const Offset(0, 0.3),
        endOffset: const Offset(0, 0),
        slideCurve: Curves.linearToEaseOut,
      ),
    );
  }

  Widget _buildWeatherScreen() {
    return BlocBuilder<WeatherListBloc, WeatherState>(
      builder: (context, state) {
        if (state is WeatherIsLoading && (state.weatherForecastList.isEmpty)) {
          return const Center(child: CircularProgressIndicator());
        } else if (state.weatherForecastList.isNotEmpty) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              FadedSlideAnimation(
                child: _buildWeatherScreenWidget(
                    state.weatherForecastList.elementAt(0)), // THE DESIGN
                beginOffset: const Offset(0, 0.3),
                endOffset: const Offset(0, 0),
                slideCurve: Curves.linearToEaseOut,
              ),
            ],
          );
        } else {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              (state is WeatherIsLoaded)
                  ? Padding(
                      padding: const EdgeInsets.only(top: 22),
                      child: Container())
                  : (state is WeatherErrorFromServer)
                      ? Container()
                      : (state is WeatherErrorFromApp)
                          ? Container()
                          : (state is WeatherNoInternet ||
                                  state is WeatherTimeOut)
                              ? Container()
                              : Container(),
            ],
          );
        }
      },
    );
  }

  Widget _buildWeatherScreenWidget(
    WeatherForecastModel weather,
  ) {
    return Column(
      children: [
        WeatherDetailWidget(weather: weather,),
        const SizedBox(
          height: 62,
        ),
        const FiveDaysForecastWidget(),
      ],
    );
  }


  Future<void> getData()async{

    BlocProvider.of<WeatherListBloc>(context).add(ResetWeatherList(
      context: context,
    ));
    BlocProvider.of<WeatherListBloc>(context)
        .add(FetchWeatherListFromQuery(context: context, params: const {
      "id": "292223",
      "units": "metric",
      "appid": ApiUrls.apiKey,
    }));

  }

}

