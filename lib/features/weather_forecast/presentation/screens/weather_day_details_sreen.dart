import 'package:animation_wrappers/animation_wrappers.dart';
import 'package:flutter/material.dart';
import 'package:weather_task/features/weather_forecast/data/models/weather_forecast_model.dart';
import 'package:weather_task/features/weather_forecast/presentation/widgets/time_weather_widget.dart';
import 'package:weather_task/features/weather_forecast/presentation/widgets/weather_detail_widgte.dart';

class WeatherDayDetails extends StatelessWidget {
  final WeatherForecastModel weather;

  const WeatherDayDetails({Key? key, required this.weather}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: _BodyWeatherDayDetails(weather: weather),
    );
  }
}

class _BodyWeatherDayDetails extends StatefulWidget {
  final WeatherForecastModel weather;

  const _BodyWeatherDayDetails({required this.weather});

  @override
  __BodyWeatherDayDetailsState createState() => __BodyWeatherDayDetailsState();
}

class __BodyWeatherDayDetailsState extends State<_BodyWeatherDayDetails> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.only(
        top: 52,
        bottom: 52,
      ),
      children: [
        WeatherDetailWidget(
          weather: widget.weather,
        ),
        const SizedBox(
          height: 42,
        ),
        FadedSlideAnimation(
          child: _buildTimesWidget(), // THE DESIGN
          beginOffset: const Offset(0, 0.3),
          endOffset: const Offset(0, 0),
          slideCurve: Curves.linearToEaseOut,
          fadeDuration: const Duration(seconds: 2),
          slideDuration: const Duration(seconds: 1),
        ),
      ],
    );
  }

  Widget _buildTimesWidget() {
    return Padding(
      padding: const EdgeInsets.only(left: 32.0, right: 32),
      child: Column(
        children: widget.weather.weatherForeastSameDayList
            .map((weather) => Padding(
              padding: const EdgeInsets.only(top: 10.0, bottom: 10),
              child: TimeWeatherWidget(weather: weather),
            ))
            .toList(),
      ),
    );
  }
}
