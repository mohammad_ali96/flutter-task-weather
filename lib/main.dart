import 'package:flutter/material.dart';

import 'core/networking/api_settings.dart';
import 'core/utils/handlers/connection_status_handler.dart';
import 'core/utils/resources.dart';
import 'core/utils/shared_preferences.dart';

import 'features/weather_forecast/presentation/screens/weather_day_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized(); // Add this
  await initApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Weather Task',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: FontsRes.segeoFont
      ),
      home: const WeatherDayScreen(),
    );
  }
}

Future<void> initApp() async {
  await SharedPref.init();
  ConnectionStatusHandler().initialize();
  ApiSettings.initHeaders();
}
